import java.util.*;


class FootballFest {
    public static void main(String args[] ) throws Exception {        
		
		LinkedList<Integer> playerIds = new LinkedList<Integer>();

        //System.out.println("Please Enter No Of Test Cases. Test Cases Must Be Less Than Or Equal To 100.");
        Scanner sc = new Scanner(System.in);
        int testCases = sc.nextInt();
		if(testCases <= 100)
		{
			for (int i = 0; i < testCases; i++) {
				//System.out.println("Please Enter No Of Passes. Passes Must Be Less Than Or Equal To 100000.");
				int passes = sc.nextInt();
				if(passes <= 100000){
					
					//System.out.println("Please Enter Initial Player Id. Id Must Be Less Than Or Equal To 1000000.");
					int initialPlayerId = sc.nextInt();
					
					if(initialPlayerId <= 1000000){
					
						playerIds.add(initialPlayerId);
			
						for (int j = 0; j < passes; j++) {
							//System.out.println("Please Enter Pass Type.");
							char passType = sc.next().trim().charAt(0);
							
							if(passType == 'P' || passType == 'p'){
								//System.out.println("Please Enter Player Id. Id Must Be Less Than Or Equal To 1000000.");
								int playerId = sc.nextInt();
								if(playerId <= 1000000){
									//System.out.println("Player "+playerId);
									playerIds.add(playerId);
								}	
							}
							else if(passType == 'B' || passType == 'b'){
								if(playerIds.size() > 1){
									int id = playerIds.get(playerIds.size() - 2);
									//System.out.println("Player "+id);
									playerIds.add(id);
								}
								// else{
									// System.out.println("B Pass Type Is Not Allowed in First Pass.");
								// }								
							}
						}
						System.out.println("Player "+playerIds.get(playerIds.size() - 1));
					}	
				}	
			}
		}        
    }
}
